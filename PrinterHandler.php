<?php 
include_once("connection.php");

/*###Class responsibilities are to execute print on the command line and store data to db###*/
class PrinterHandler{

   //Declaring variables   
   private $data; //data for internal use
   private $result; //data for external use

   //Constructor
   public function __construct($data) {
      $this->data = $data;
   }

   //Execute pritn and report back
   public function executePrint() {
      $jobId = $this->_createAndExecuteOrder(); 

      if( !empty($jobId) ) {
         $printed = $this->_getStatus($jobId);

         if($printed) {
            $this->result['success'] = true;
            $this->result['jobId'] = $jobId; //store job id
            $this->result['pages-left'] = intval( $_SESSION['pages-left'] ) - $this->data['total']; //store new pages-left value
            //Save file to db first
            $this->_logPrint();
            $this->_clearFile();

         } else { //job is waiting in que
            $this->result['success'] = false;
            $this->result['jobId'] = $jobId; 
            $this->result['rank'] = $this->data['rank'];
            $this->result['pages-left'] = intval( $_SESSION['pages-left'] ) - $this->data['total']; //store new pages-left value
            $this->_logPrint();
            $this->_clearFile();
         }

      } else {  //No job ID found
         $this->result['success'] = false; 
         $this->result['command'] = $this->data['command'];
         $this->result['error'] = "Cannot retrieve a Job ID.";
         $this->_clearFile();
      }
      return $this->result;
   }

   //Create print command and execute 
   private function _createAndExecuteOrder() {
      $printerName = $this->data['printer-name'];
      $copies = $this->data['copies'];
      $layout = ($this->data['layout'] == 'landscape') ? 'landscape' : 'portrait' ;
      $rangeOption = ( empty($this->data['raw-range']) ) ? '' : '-o page-ranges=' . $this->data['raw-range'];
      $twoSided = $this->data['twoSided'];
      $filePath = "upload/" . $_SESSION['encrypted-name'] ; 

      /*
         Specify printer name " -d printername "
         Multiple copies option -n num-copies 
         Two-side printing -o sides=two-sided-long-edge/two-sided-short-edge
         Landscape option -o landscape
         Page ranges -o page-ranges=1-4,7,9-12
         Add file path
      */
      //$command = "lpr -P $printerName -#$copies -o sides=$twoSided -o $layout $rangeOption $filePath" ; //does not work over network  -d $printerName
      $command = "lp -n $copies -o sides=$twoSided -o $layout $rangeOption $filePath" ;   
      //$command = "lp -d $printerName -n $copies -o sides=$twoSided -o $layout $rangeOption" ; //bad command 
      
      //Reporting
      $this->data['command'] = $command; //save for mysql storage

      exec($command); //excecute print command
      exec('lpstat -o $printername | awk \'END {print $1} \' ', $response ); //get response
      $jobId = substr($response[0], strrpos($response[0], '-') + 1); //get job id from response

      //Return response from printer
      return $jobId;
   }

   //Log to database
   private function _logPrint() {
      $con = getCon();

      $query = 'INSERT INTO `PRINTING`.`print_log` (`SJSUID`,
                                                `FirstName`,
                                                `LastName`,
                                                `JobID`,
                                                `PagesUsed`,
                                                `PagesLeft`,
                                                `PrintCommand`) VALUES (?,?,?,?,?,?,?)';
      
      $MEMID = $_SESSION['_USER_']['MemberID'];
      $SJSUID = $_SESSION['_USER_']["SJSUID"];
      $JOBID = $this->result['jobId'];
      $FIRSTNAME = $_SESSION['_USER_']['FirstName'];
      $LASTNAME = $_SESSION['_USER_']['LastName'];
      $USED = $this->data['total'];
      $LEFT =  $this->result['pages-left'];
      $COMMAND = $this->data['command'];
      
      //prepare stmt
      $stmt = $con->prepare($query); 
      if($stmt === false) {
        trigger_error('Wrong SQL: ' . $query . ' Error: ' . $con->error, E_USER_ERROR);
      }

      /* Bind parameters. TYpes: s = string, i = integer, d = double,  b = blob */
      if( !$stmt->bind_param('ssssiis', $SJSUID, $FIRSTNAME, $LASTNAME, $JOBID, $USED, $LEFT, $COMMAND) ){
         echo ( "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error ); 
      }

      /* Execute statement */
      if( !$stmt->execute() ) {
         echo ( "Execute failed: (" . $stmt->errno . ") " . $stmt->error );
      }

      $this->result["insert_id"] = $stmt->insert_id;
      $this->result["affected_rows"] = $stmt->affected_rows;

      $stmt->close();

      //Update pages left
      if( !$con->query("UPDATE `PRINTING`.`print_allowance` SET `Allowance` = $LEFT WHERE `MemID` = $MEMID") ){
         echo( "Table creation failed: (" . $con->errno . ") " . $con->error );
      }

      $con->close();
   }
   
   //Clear file from upload directory
   private function _clearFile() {
      unlink( "upload/" . $_SESSION['encrypted-name'] ); //unlink file from upload directory
   }

   //Determine status of job
   private function _getStatus($jobId) {
      $completed = false;
      $endTime = time() + 1 ; //timer for counting how long is left
      
      while( time() < $endTime ) {
         time_nanosleep( 0 , 250000000 ); //delay 
         exec( 'lpq -P ' . $this->data['printer-name'] . ' | awk \'/ '.$jobId.' /{print $1}\' ' , $result ); //check if job is completed 

         $this->data['rank'] = $result; //store for rank
         if( empty($result) ) {//print has been cleared (sent/removed) from queue
            $completed = true;
            break;
         } else { 
            $result = ""; //clear variable
         }
      }
      return $completed;
   }
}
?>